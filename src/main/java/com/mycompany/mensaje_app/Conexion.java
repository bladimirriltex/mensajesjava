/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mensaje_app;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Edú
 */
public  class Conexion {
    public  Connection getConexion()
    {
        Connection conection=null;
        try {
            conection=DriverManager.getConnection("jdbc:mysql://localhost:3306/mensajes_app?serverTimezone=UTC","root","");
            if(conection!=null)
            {
                System.out.println("conexion exitosa");
            }else{
                System.out.println("Fallo de conexion");
            }
        } catch (SQLException e) {
            System.out.println("conexion:" + e);
        }
        
        return conection;
    }
}
